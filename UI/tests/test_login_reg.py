import time
from UI.pages.base_page import BasePage
from UI.pages.registration_page_adv import RegPage
from selenium.webdriver.common.by import By


class TestAdvReg:
    def test_login_adv_page(self, driver):
        login_page = RegPage(driver, 'http://adv.kadam-docker-new.sdev.pw/en/login')
        login_page.open()
        login_page.login_adv()

    def test_login_pub_page(self, driver):
        login_page = RegPage(driver, 'http://pub.kadam-docker-new.sdev.pw/en/login')
        login_page.open()
        login_page.login_pub()
        time.sleep(5)

    def test_registration_adv(self, driver):
        reg_page = RegPage(driver, 'http://adv.kadam-docker-new.sdev.pw/en/reg')
        reg_page.open()
        reg_page.reg_adv_pub()

    def test_registration_pub(self, driver):
        reg_page = RegPage(driver, 'http://pub.kadam-docker-new.sdev.pw/en/reg')
        reg_page.open()
        reg_page.reg_adv_pub()
