import time
from UI.locators.login_page_locators import RegistrationPageLocators
from UI.pages.base_page import BasePage


class RegPage(BasePage):
    locators = RegistrationPageLocators()

    def login_adv(self):
        self.element_is_clickable(self.locators.ACCEPT_COOKIE).click()
        self.element_is_visible(self.locators.MAIL).send_keys("advert@smartmail.com")
        self.element_is_visible(self.locators.PASSWORD).send_keys("advert@smartmail.com")
        self.element_is_clickable(self.locators.SIGN_IN_BUTTON).click()

    def login_pub(self):
        self.element_is_clickable(self.locators.ACCEPT_COOKIE).click()
        self.element_is_visible(self.locators.MAIL).send_keys("webmaster@smartmail.com")
        self.element_is_visible(self.locators.PASSWORD).send_keys("webmaster@smartmail.com")
        self.element_is_clickable(self.locators.SIGN_IN_BUTTON).click()

    def reg_adv_pub(self):
        self.element_is_clickable(self.locators.ACCEPT_COOKIE).click()
        self.element_is_visible(self.locators.INPUT_MAIL).send_keys("test123@mail.ru")
        self.element_is_visible(self.locators.INPUT_NAME).send_keys("name")
        self.element_is_visible(self.locators.INPUT_PASSWORD).send_keys("qwe123")
        self.element_is_visible(self.locators.INPUT_CONFIRM_PASSWORD).send_keys("qwe123")
        self.element_is_clickable(self.locators.CURRENCY_RUB).click()
        self.element_is_visible(self.locators.INPUT_COMMUNICATION_FORM).send_keys("qweqweqweqwe")
        self.element_is_visible(self.locators.SELECT_HOWABOUTUS_FORM).click()
        self.element_is_visible(self.locators.SELECT_FROM_FRIEND).click()
        self.element_is_present(self.locators.CHECKBOX_AGREEMENT).click()
        time.sleep(5)
        self.element_is_visible(self.locators.SIGN_UP_BUTTON).click()
