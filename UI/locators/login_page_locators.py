from selenium.webdriver.common.by import By


class RegistrationPageLocators:

    # login page
    ACCEPT_COOKIE = (By.XPATH, "//div[@class='flex']/button[3]")
    REJECT_COOKIE = (By.XPATH, "//div[@class='flex']/button[2]")
    MAIL = (By.ID, "email")
    PASSWORD = (By.ID, "password")
    SIGN_IN_BUTTON = (By.XPATH, '//div[@class="btnwrap btnwrap_h-56 btnwrap_w-280"]/button')
    REG_BUTTON = (By.XPATH, '//a[@class="btn btn_transparent _regButton"]')
    FORGOT_PASSWORD_BUTTON = (By.ID, "password-recovery")
    PASSWORD_RECOVERY_EMAIL = (By.ID, "password_recovery_email")
    PASSWORD_RECOVERY_SEND = (By.XPATH, '//div[@class="btnwrap btnwrap_h-56 btnwrap_w-max"]')

    # registration page for adv and pub new ui

    INPUT_MAIL = (By.XPATH, "//input[@class='form-inputs__input emailInput']")
    INPUT_NAME = (By.XPATH, "//input[@id='displayName']")
    INPUT_PASSWORD = (By.XPATH, "//input[@id='password']")
    INPUT_CONFIRM_PASSWORD = (By.XPATH, "//input[@id='password2']")
    CURRENCY_DOL = (By.XPATH, "//label[@for='currencyRadioDol']")
    CURRENCY_RUB = (By.XPATH, "//label[@for='currencyRadioRub']")
    # выбираем способы только после клика на форму
    SELECT_COMMUNICATION_FORM = (By.XPATH, "//div[@class='form-select']")
    SELECT_TELEGRAM = (By.XPATH, "//div[@class='form-select']/div[2]/div[1]")
    SELECT_SKYPE = (By.XPATH, "//div[@class='form-select']/div[2]/div[2]")
    SELECT_PHONE = (By.XPATH, "//div[@class='form-select']/div[2]/div[3]")
    INPUT_COMMUNICATION_FORM = (By.XPATH, "//input[@id='connectionType']")
    # выбираем способы только после клика на форму 2
    SELECT_HOWABOUTUS_FORM = (By.XPATH, "//div[@class='select-know']")
    SELECT_FROM_FRIEND = (By.XPATH, "//div[@class='select-know']/div[2]/div[1]")
    SELECT_CONTEXT_AD = (By.XPATH, "//div[@class='select-know']/div[2]/div[2]")
    SELECT_AD = (By.XPATH, "//div[@class='select-know']/div[2]/div[3]")
    SELECT_CONFERENCES = (By.XPATH, "//div[@class='select-know']/div[2]/div[4]")
    SELECT_SEARCH = (By.XPATH, "//div[@class='select-know']/div[2]/div[5]")
    SELECT_SOCIAL_NETWORK = (By.XPATH, "//div[@class='select-know']/div[2]/div[6]")
    SELECT_THEMATIC_SITES = (By.XPATH, "//div[@class='select-know']/div[2]/div[7]")
    SELECT_OTHER = (By.XPATH, "//div[@class='select-know']/div[2]/div[8]")
    CHECKBOX_AGREEMENT = (By.XPATH, "//div[@class='checkbox-form']/input")
    SIGN_UP_BUTTON = (By.XPATH, "//button[@class='btn btn_red btn-form-submit btnGtagRegist _regButton']")

