stats_get_payload = {"id": None, "name": None, "groups": ["time_day", "traffic_format", "advertiser_campaign"],
                     "metrics": ["finance_moneyOut", "conversion_conversions", "conversion_holds", "conversion_rejects",
                                 "advertiser_cpl", "advertiser_cpa", "advertiser_ROI"],
                     "filters": {"filters": [{"id": "traffic_format", "type": "list", "include": [10], "exclude": []}],
                                 "period": "yesterday", "timezone": -5},
                     "compare": {"period": "yesterday", "sort": False, "mode": "diff"}}

stats_share_payload = {"groups": ["time_day", "traffic_format", "advertiser_campaign"],
                       "metrics": ["finance_moneyOut", "conversion_conversions", "conversion_holds",
                                   "conversion_rejects", "advertiser_cpl", "advertiser_cpa", "advertiser_ROI"],
                       "filters": {"filters": [{"id": "traffic_format", "type": "list", "include": [10], "exclude": [],
                                                "forLabel": [{"id": 10, "label": "teaser"}]}], "period": "yesterday",
                                   "timezone": -5}, "compare": {"period": "yesterday", "sort": False, "mode": "diff"}}

site_stats_filters_payload = {"isTrusted": True,
                              "filters": {"dateFrom": "2024-03-05", "dateTo": "2024-03-05", "timezone": -11,
                                          "view": "bids", "adsIds": []}, "perPage": 20,
                              "page": 1}

postback_stats_payload = {"isTrusted": True,
                          "filters": {"dateFrom": "2024-03-04", "dateTo": "2024-03-04", "timezone": -12,
                                      "showArchive": False, "showRetargeting": True, "adsIds": []}, "perPage": 20,
                          "page": 1}

extended_data_payload = {"filters": {"dateFrom": "2024-03-05", "dateTo": "2024-03-05"}, "pathIds": []}
