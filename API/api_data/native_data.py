native_create_payload = {
    "type": 10,
    "folderId": None,
    "name": "create_test",
    "cpType": 0,
    "isConversionFromPostback": 0,
    "isGoodTraffic": 1,
    "url": "https://asdasd.com/?click_id={click_id}",
    "conversion": None,
    "bids": [
        {
            "bid": "0.1",
            "leadCost": 0,
            "countries": [
                48
            ]
        }
    ],
    "cities": {
        "mode": 1,
        "list": [
            "city_11747832"
        ]
    },
    "isps": {
        "mode": 1,
        "list": []
    },
    "platformVersions": [
        1
    ],
    "devices": [
        1
    ],
    "browsers": [
        1
    ],
    "languages": [
        1
    ],
    "connectionType": 1,
    "categories": [
        1001
    ],
    "newAudiences": [],
    "audiences": {
        "mode": 1,
        "list": None
    },
    "sites": {
        "mode": 0,
        "list": []
    },
    "ips": {
        "mode": 0,
        "list": [
            "255.255.255.0 - 255.255.255.1"
        ]
    },
    "disableProxy": 1,
    "commonMoneyLimit": 0,
    "dayMoneyLimit": 1000,
    "isEvenDistribution": None,
    "totalLossLimit": 0,
    "materialViews": {
        "count": 0,
        "days": 0
    },
    "campaignView": {
        "count": 0,
        "days": 0
    },
    "minBlockViews": 0,
    "maxBlockViews": 0,
    "dayClickLimit": 0,
    "dayConversionsLimit": 0,
    "startDate": None,
    "stopDate": None,
    "time": {
        "mode": 1,
        "list": [
            {
                "day": 1,
                "hours": [
                    0,
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7,
                    8,
                    9,
                    10,
                    11,
                    12,
                    13,
                    14,
                    15,
                    16,
                    17,
                    18,
                    19,
                    20,
                    21,
                    22,
                    23
                ]
            },
            {
                "day": 2,
                "hours": [
                    0,
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7,
                    8,
                    9,
                    10,
                    11,
                    12,
                    13,
                    14,
                    15,
                    16,
                    17,
                    18,
                    19,
                    20,
                    21,
                    22,
                    23
                ]
            },
            {
                "day": 3,
                "hours": [
                    0,
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7,
                    8,
                    9,
                    10,
                    11,
                    12,
                    13,
                    14,
                    15,
                    16,
                    17,
                    18,
                    19,
                    20,
                    21,
                    22,
                    23
                ]
            },
            {
                "day": 4,
                "hours": [
                    0,
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7,
                    8,
                    9,
                    10,
                    11,
                    12,
                    13,
                    14,
                    15,
                    16,
                    17,
                    18,
                    19,
                    20,
                    21,
                    22,
                    23
                ]
            },
            {
                "day": 5,
                "hours": [
                    0,
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7,
                    8,
                    9,
                    10,
                    11,
                    12,
                    13,
                    14,
                    15,
                    16,
                    17,
                    18,
                    19,
                    20,
                    21,
                    22,
                    23
                ]
            },
            {
                "day": 6,
                "hours": [
                    0,
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7,
                    8,
                    9,
                    10,
                    11,
                    12,
                    13,
                    14,
                    15,
                    16,
                    17,
                    18,
                    19,
                    20,
                    21,
                    22,
                    23
                ]
            },
            {
                "day": 7,
                "hours": [
                    0,
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7,
                    8,
                    9,
                    10,
                    11,
                    12,
                    13,
                    14,
                    15,
                    16,
                    17,
                    18,
                    19,
                    20,
                    21,
                    22,
                    23
                ]
            }
        ]
    },
    "timezone": -3,
    "allowMultiAds": 1,
    "interests": {
        "mode": 1,
        "list": None
    },
    "gender": 3,
    "age": [
        1
    ],
    "impTracker": "1",
    "macrosGroups": {
        "mode": 0,
        "include": [],
        "exclude": []
    },
    "ssps": {
        "mode": 1,
        "list": []
    },
    "hasCorrectPostback": 1,
    "forecastMinBid": None,
    "forecastMaxBid": None
}

native_edit_payload = {"type": 10, "folderId": None, "name": "editing", "cpType": 0, "isConversionFromPostback": None,
                       "isGoodTraffic": 0,
                       "url": "https://asdasddsa.com/?block_id={block_id}&country_code={country_code}",
                       "conversion": None, "bids": [{"bid": "1.1", "leadCost": 0,
                                                     "countries": [48, 193, 225, 226, 227, 228, 229, 230, 231, 232, 233,
                                                                   234, 235, 236,
                                                                   237, 238, 239, 240, 241, 276, 288, 290, 292, 300]}],
                       "cities": {"mode": 1, "list": ["city_11747832"]}, "isps": {"mode": 1, "list": []},
                       "platformVersions": [1, 2], "devices": [50], "browsers": [1, 4], "languages": [1],
                       "connectionType": 2,
                       "categories": [1001, 134], "newAudiences": [], "audiences": {"mode": 0, "list": []},
                       "sites": {"mode": 0, "list": []}, "ips": {"mode": 0, "list": ["255.255.255.0 - 255.255.255.1"]},
                       "disableProxy": 1, "commonMoneyLimit": 0, "dayMoneyLimit": 1000, "isEvenDistribution": 0,
                       "totalLossLimit": 0,
                       "materialViews": {"count": 0, "days": 0}, "campaignView": {"count": 0, "days": 0},
                       "minBlockViews": 0,
                       "maxBlockViews": 0, "dayClickLimit": 0, "dayConversionsLimit": 0,
                       "startDate": "2024-02-24 10:50:00",
                       "stopDate": None, "time": {"mode": 1, "list": [
        {"day": 1, "hours": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]},
        {"day": 2, "hours": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]},
        {"day": 3, "hours": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]},
        {"day": 4, "hours": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]},
        {"day": 5, "hours": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]},
        {"day": 6, "hours": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]},
        {"day": 7, "hours": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]}]},
                       "timezone": -3, "allowMultiAds": 0, "interests": {"mode": 0, "list": []}, "gender": 1,
                       "age": [1, 4, 8, 16, 32],
                       "maxCpm": "0.00", "impTracker": "1", "macrosGroups": {"mode": 0, "include": [], "exclude": []},
                       "ssps": {"mode": 0, "list": []},
                       "isApplyNewDomainToAds": 0, "hasCorrectPostback": 1, "forecastMinBid": None,
                       "forecastMaxBid": None}
