from API.api_data.cookie import headers
import requests

#  to start tests - python -m pytest -v API/ in test/

# url = 'https://partners.kadam.net/'
url = 'http://adv.kadam-docker-new.sdev.pw/'


# универсальная функция создания рк, передается соответствующий рк пейлоад
def create_new_campaign(payload):
    response = requests.post(f'{url}api/campaigns/create',
                             json=payload, headers=headers).json()
    return response.get('data').get('id')


def check_campaign(campaign_id):
    return requests.get(f'{url}api/campaigns/{campaign_id}', headers=headers).json()


def edit_campaign(payload):
    response = requests.post(f'{url}api/campaigns/create',
                             json=payload, headers=headers).json()
    return response


def get_last_archived_id():
    archive = requests.post(f'{url}api/campaigns', json={"filters": {"archive": 1}},
                            headers=headers).json()
    return archive.get('data').get('rows')[0].get('campaign').get('id')


def to_archive(campaign_id):
    return requests.post(f'{url}api/campaigns/archive',
                         json={"campaignsIds": [f'{campaign_id}']}, headers=headers).json()


def reg_new_adv(payload):
    return requests.post(f'{url}en/reg', json=payload)


def stats_adv_get(payload):
    return requests.post(f'{url}api/custom-reports/data', json=payload, headers=headers).json()


def stats_adv_share(payload):
    return requests.post(f'{url}api/custom-reports/share', json=payload, headers=headers).json()


def stats_adv_share_check(export_id):
    return requests.get(f'{url}api/custom-reports/share/{export_id}', headers=headers).json()


def site_stats_get():
    return requests.get(f'{url}api/stats/site', headers=headers).json()


def site_stats_filters(payload):
    return requests.post(f'{url}api/stats/site', json=payload, headers=headers).json()


def postback_stats_get(payload):
    return requests.post(f'{url}api/stats/postback', json=payload, headers=headers).json()


def extended_stats_get():
    return requests.options(f'{url}api/stats/extended', headers=headers).json()


def extended_stats_campaign(payload):
    return requests.post(f'{url}api/stats/extended', json=payload, headers=headers).json()
# x = requests.get(f'{url}api/custom-reports/share/{export_id}', headers=headers).json()
# print(x.get('data').get('metrics'))
