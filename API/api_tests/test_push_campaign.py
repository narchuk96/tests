from API.api_data.push_data import push_create_payload, push_edit_payload
from API.api_functions.functions import get_last_archived_id, to_archive, create_new_campaign, edit_campaign, check_campaign


def test_create_push():
    campaign_id = create_new_campaign(push_create_payload)
    assert check_campaign(campaign_id).get('data').get('type')
    to_archive(campaign_id)


def test_edit_push():
    campaign_id = create_new_campaign(push_create_payload)
    push_edit_payload["campaignId"] = campaign_id
    response = edit_campaign(push_edit_payload)
    assert response.get('code') == 200 and response.get('data').get('id') == campaign_id
    to_archive(campaign_id)


def test_archive_push():
    campaign_id = create_new_campaign(push_create_payload)
    response = to_archive(campaign_id)
    assert response.get('code') == 200 and get_last_archived_id() == campaign_id
