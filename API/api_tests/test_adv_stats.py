from API.api_data.stats_data import stats_get_payload, stats_share_payload, site_stats_filters_payload, postback_stats_payload, extended_data_payload
from API.api_data.native_data import native_create_payload
from API.api_functions.functions import (stats_adv_get, stats_adv_share, site_stats_get, postback_stats_get,
                                         stats_adv_share_check, create_new_campaign, site_stats_filters, to_archive, extended_stats_get, extended_stats_campaign)


def test_adv_stats_get():
    response = stats_adv_get(stats_get_payload)
    assert response.get('code') == 200
    assert type(response.get('data').get('rows')) == list


def test_adv_stats_share():
    response = stats_adv_share(stats_share_payload)
    export_id = response.get('data').get('id')
    # проверка, что получили айдишник (не пустую строку)
    assert len(response.get('data').get('id')) != 0
    # проверка, что получили список метрик по айдишнику
    assert (stats_adv_share_check(export_id).get('data').get('metrics'))


def test_adv_site_stats_get():
    # проверяем респонс на наличие элемента тотал
    assert site_stats_get().get('data').get('total')


def test_adv_site_stats_filters_check():
    # передаем в стату по площадкам некоторые фильтры (айди новосозданной рк, итд)
    campaign_id = create_new_campaign(native_create_payload)
    site_stats_filters_payload["filters"]["campaignIds"] = [campaign_id] # прикол с фильтрами - сюда можно передать и int и list
    assert site_stats_filters(site_stats_filters_payload).get('data').get('total')
    #print(site_stats_filters(site_stats_filters_payload))
    to_archive(campaign_id)


def test_adv_postback_stats_get():
    campaign_id = create_new_campaign(native_create_payload)
    postback_stats_payload["filters"]["campaignIds"] = [campaign_id]  # сюда ТОЛЬКО список
    assert postback_stats_get(postback_stats_payload).get('data').get('columns')
    to_archive(campaign_id)


def test_adv_extended_stats_get():
    assert extended_stats_get().get('data').get('views')


def test_adv_extended_stats_campaign_filter():
    campaign_id = create_new_campaign(native_create_payload)  # сюда тоже только список (однако в ui они все 3 отличаются)
    extended_data_payload["campaignIds"] = [campaign_id]
    assert extended_stats_campaign(extended_data_payload).get('data').get('rows')
    to_archive(campaign_id)