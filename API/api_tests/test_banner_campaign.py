from API.api_data.banner_data import banner_edit_payload, banner_create_payload
from API.api_functions.functions import get_last_archived_id, create_new_campaign, to_archive, edit_campaign, check_campaign


def test_create_banner():
    campaign_id = create_new_campaign(banner_create_payload)
    assert check_campaign(campaign_id).get('data').get('type')
    to_archive(campaign_id)


def test_edit_banner():
    campaign_id = create_new_campaign(banner_create_payload)
    banner_edit_payload["campaignId"] = campaign_id
    response = edit_campaign(banner_edit_payload)
    assert response.get('code') == 200 and response.get('data').get('id') == campaign_id
    to_archive(campaign_id)


def test_archive_banner():
    campaign_id = create_new_campaign(banner_create_payload)
    response = to_archive(campaign_id)
    assert response.get('code') == 200 and get_last_archived_id() == campaign_id
