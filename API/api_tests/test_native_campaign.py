from API.api_data.native_data import native_edit_payload, native_create_payload
from API.api_data.cookie import headers
import requests
from API.api_functions.functions import get_last_archived_id, to_archive, create_new_campaign, edit_campaign, check_campaign


def test_get_list_campaigns():
    #response = requests.get('https://partners.kadam.net/api/campaigns', headers=headers).json()
    response = requests.get('http://adv.kadam-docker-new.sdev.pw/api/campaigns', headers=headers).json()
    assert response.get('code') == 200


def test_create_native():
    campaign_id = create_new_campaign(native_create_payload)
    assert check_campaign(campaign_id).get('data').get('type')
    to_archive(campaign_id)


def test_edit_native():
    campaign_id = create_new_campaign(native_create_payload)
    native_edit_payload["campaignId"] = campaign_id
    response = edit_campaign(native_edit_payload)
    assert response.get('code') == 200 and response.get('data').get('id') == campaign_id
    to_archive(campaign_id)


def test_archive_native():
    campaign_id = create_new_campaign(native_create_payload)
    response = to_archive(campaign_id)
    assert response.get('code') == 200 and get_last_archived_id() == campaign_id
